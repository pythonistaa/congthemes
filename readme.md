## Abstract
The repository contains public UI themes for best git client [Cong](http://www.cong.tools "Cong").

## How to use
1. Clone the repo (**git clone https://bitbucket.org/pythonistaa/congthemes.git**) or just download the liked theme (entire folder).
1. Copy all files from downloaded theme folder to **\ApplicationData\Themes\Default** folder of your Cong installation (replacing the existing files).
1. Restart Cong.
1. Enjoy!

## How to add
1. Clone the repo if needed (**git clone https://bitbucket.org/pythonistaa/congthemes.git**)
1. Create your own branch called **/author_name/theme_name**
1. Add new theme subfolder with unique name **theme_name** in the **Custom** folder
1. Use the existing theme as sample or create new theme from scratch
	**Note:** The new theme must have the same structure as any predefined theme (including tags set)
1. Add or update the screenshot image of your theme. The screenshot should be as complete as possible. I recommend that you open the Commits tab, select some commit and file in it so that the difference is displayed.
1. Also add or update the screenshot in this file (**readme.md**)
1. Commit and push your changes
1. Create a pull-request to **master** branch
	**Note:** A direct push to master branch is forbidden to premoderate.

**Important:** Please do not modify the themes of other authors, create your own.

## Screenshots

### Predefined Themes

#### Light (Evgeny Kralko, support@cong.tools)
![Light](https://bitbucket.org/pythonistaa/congthemes/raw/07c5b2a6f90624c0d0015b1e744d66369bb6aad5/Predefined/Light/Light.png)

#### Dark (Evgeny Kralko, support@cong.tools)
![Dark](https://bitbucket.org/pythonistaa/congthemes/raw/07c5b2a6f90624c0d0015b1e744d66369bb6aad5/Predefined/Dark/Dark.png)

#### Darcong (Dmitry Kasatsky, support@cong.tools)
![Darcong](https://bitbucket.org/pythonistaa/congthemes/raw/07c5b2a6f90624c0d0015b1e744d66369bb6aad5/Predefined/Darcong/Darcong.png)

#### ConEmu (Evgeny Kralko, support@cong.tools)
![ConEmu](https://bitbucket.org/pythonistaa/congthemes/raw/07c5b2a6f90624c0d0015b1e744d66369bb6aad5/Predefined/ConEmu/ConEmu.png)

### Custom Themes
Be the first here!
**Note:** Do not forget to specify the author name and email for communication.